const db = require("../models");
const Product = db.product;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.create = (req, res) => {
    // Validate request
    // if (!req.body.title) {
    //   res.status(400).send({
    //     message: "Content can not be empty!"
    //   });
    //   return;
    // }
  
    // Create a product
    const product = {
        product_id: req.body.product_id,
        product_name: req.body.product_name,
        product_keyword: req.body.product_keyword,
        product_price: req.body.product_price,
        product_desc: req.body.product_desc,
      
    };
  
    // Save Tutorial in the database
    Product.create(product)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the pr."
        });
      });
  };

// Retrieve all Product from the database.


  exports.findAll = (req,res) => {
    return Product.findAll({
     
    }).then((products) => {
      res.send(products);
    });
  };

