const db = require("../models");
const Cart = db.cart;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.create = (req, res) => {
    // Validate request
    // if (!req.body.title) {
    //   res.status(400).send({
    //     message: "Content can not be empty!"
    //   });
    //   return;
    // }
  
    // Create a Cart
    const cart = {
        cart_id: req.body.cart_id,
        product_id: req.body.product_id,
        ip_address: req.body.ip_address,
        customer_id: req.body.customer_id,
        product_price: req.body.product_price,
        total_amount: req.body.total_amount,
        product_img: req.body.product_img,
        product_quantity: req.body.product_quantity,
    };
  
    // Save Cart in the database
    Cart.create(cart)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the cart."
        });
      });
  };

// Retrieve all Cart from the database.


  exports.findAll = (req,res) => {
    return Cart.findAll({
     
    }).then((carts) => {
      res.send(carts);
    });
  };

