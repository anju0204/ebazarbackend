const db = require("../models");
const Category = db.category;
const Op = db.Sequelize.Op;

// Create and Save a new Tutorial
exports.create = (req, res) => {
    // Validate request
    // if (!req.body.title) {
    //   res.status(400).send({
    //     message: "Content can not be empty!"
    //   });
    //   return;
    // }
  
    // Create a Cart
    const category = {
        cat_id: req.body.cat_id,
        cat_title: req.body.cat_title,
    };
  
    // Save Cart in the database
    Category.create(category)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the cart."
        });
      });
  };

// Retrieve all Cart from the database.


  exports.findAll = (req,res) => {
    return Category.findAll({
     
    }).then((categories) => {
      res.send(categories);
    });
  };

