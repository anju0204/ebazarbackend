exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.success!.");
  };
  
  exports.userBoard = (req, res) => {
    res.status(200).send("User Content.success!");
  };
  
  exports.adminBoard = (req, res) => {
    res.status(200).send("Admin Content.success!");
  };
  
  exports.moderatorBoard = (req, res) => {
    res.status(200).send("Moderator Content.success!");
  };