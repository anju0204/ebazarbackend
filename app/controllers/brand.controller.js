const db = require("../models");
const Brand = db.brand;
const Op = db.Sequelize.Op;

// Create and Save a new brand
exports.create = (req, res) => {
    // Validate request
    // if (!req.body.title) {
    //   res.status(400).send({
    //     message: "Content can not be empty!"
    //   });
    //   return;
    // }
  
    // Create a brand
    const brand = {
        brand_id: req.body.brand_id,
        brand_title: req.body.brand_title,
        
      
    };
  
    // Save Tutorial in the database
    Brand.create(brand)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the brand."
        });
      });
  };

// Retrieve all brand from the database.


  exports.findAll = (req,res) => {
    return Brand.findAll({
     
    }).then((brands) => {
      res.send(brands);
    });
  };

