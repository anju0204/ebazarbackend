module.exports = (sequelize, Sequelize) => {
    const Category = sequelize.define("category", {
        cat_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      cat_title:{
        type: Sequelize.STRING
      }
    });
  
    return Category;
  };