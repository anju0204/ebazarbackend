module.exports = (sequelize, Sequelize) => {
    const Brand = sequelize.define("brand", {
        brand_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
    
      brand_title: {
        type: Sequelize.STRING
      }
     
    });
  
    return Brand;
  };