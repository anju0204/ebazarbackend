module.exports = (sequelize, Sequelize) => {
    const Cart = sequelize.define("cart", {
        cart_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      ip_address: {
        type: Sequelize.STRING
      },
      customer_id: {
        type: Sequelize.STRING
      },
      product_name: {
        type: Sequelize.STRING
      },
     
      total_amount:{
        type: Sequelize.STRING
      },
      product_price:{
        type: Sequelize.STRING
      },
      product_img:{
        type: Sequelize.STRING
      },
      product_quantity:{
        type: Sequelize.STRING
      }
    });
  
    return Cart;
  };