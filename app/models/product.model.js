module.exports = (sequelize, Sequelize) => {
    const Product = sequelize.define("product", {
        product_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
    
      product_name: {
        type: Sequelize.STRING
      },
     
      product_keyword:{
        type: Sequelize.STRING
      },
      product_price:{
        type: Sequelize.STRING
      },
      product_desc:{
        type: Sequelize.STRING
      }
    });
  
    return Product;
  };