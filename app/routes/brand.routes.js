module.exports = app => {
    const brands = require("../controllers/brand.controller.js");
  
    var router = require("express").Router();
  
    // Create a new carts
    router.post("/", brands.create);
  
    // Retrieve all carts
    router.get("/", brands.findAll);
  
    // // Retrieve all published Tutorials
    // router.get("/published", tutorials.findAllPublished);
  
    // // Retrieve a single Tutorial with id
    // router.get("/:id", tutorials.findOne);
  
    // // Update a Tutorial with id
    // router.put("/:id", tutorials.update);
  
    // // Delete a Tutorial with id
    // router.delete("/:id", tutorials.delete);
  
    // // Create a new Tutorial
    // router.delete("/", tutorials.deleteAll);
  
     app.use('/api/brands', router);
  };