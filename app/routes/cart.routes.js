module.exports = app => {
    const carts = require("../controllers/cart.controller.js");
  
    var router = require("express").Router();
  
    // Create a new carts
    router.post("/", carts.create);
  
    // Retrieve all carts
    router.get("/", carts.findAll);
  
    // // Retrieve all published Tutorials
    // router.get("/published", tutorials.findAllPublished);
  
    // // Retrieve a single Tutorial with id
    // router.get("/:id", tutorials.findOne);
  
    // // Update a Tutorial with id
    // router.put("/:id", tutorials.update);
  
    // // Delete a Tutorial with id
    // router.delete("/:id", tutorials.delete);
  
    // // Create a new Tutorial
    // router.delete("/", tutorials.deleteAll);
  
     app.use('/api/carts', router);
  };